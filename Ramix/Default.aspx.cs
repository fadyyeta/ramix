﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void SendMailSSL()
        {
            var myPass = "P@ssW0rdRamix";
            MailMessage m = new MailMessage();
            SmtpClient sc = new SmtpClient();
            m.From = new MailAddress(txtFrom.Text);
            m.To.Add(txtTo.Text);
            m.Subject = "This is a test";
            m.Body = "This is a sample message using SMTP authentication";
            sc.Host = txtMailServer.Text;
            string str1 = "gmail.com";
            string str2 = txtFrom.Text.ToLower();
            if (str2.Contains(str1))
            {
                try
                {
                    sc.Port = 587;
                    sc.Credentials = new System.Net.NetworkCredential(txtFrom.Text, myPass);
                    sc.EnableSsl = true;
                    sc.Send(m);
                    Response.Write("Email Send successfully");
                }
                catch (Exception ex)
                {
                    Response.Write("<BR><BR>* Please double check the From Address and Password to confirm that both of them are correct. <br>");
                    Response.Write("<BR><BR>If you are using gmail smtp to send email for the first time, please refer to this KB to setup your gmail account: http://www.smarterasp.net/support/kb/a1546/send-email-from-gmail-with-smtp-authentication-but-got-5_5_1-authentication-required-error.aspx?KBSearchID=137388");
                    Response.End();
                    throw ex;
                }
            }
            else
            {
                try
                {
                    sc.Port = 8889;
                    sc.Credentials = new System.Net.NetworkCredential(txtFrom.Text, myPass);
                    sc.EnableSsl = false;
                    sc.Send(m);
                    Response.Write("Email Send successfully");
                }
                catch (Exception ex)
                {
                    Response.Write("<BR><BR>* Please double check the From Address and Password to confirm that both of them are correct. <br>");
                    Response.End();
                    throw ex;
                }
            }
        }

        protected void send_message_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //SendMail();
                SendMailSSL();
            }
        }
    }
}