﻿<%@ WebService Language="C#" Class="Ramix.WebService" %>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Net.Mail;

namespace Ramix
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        [WebMethod]
        public void SendMail(string bodyForm)
        {
            string fromAddress = "postmaster@ramixpharma.com";
            string fromPassword = "P@ssW0rdRamix";
            string subject = "Email from Ramix ";
            string body = bodyForm;
            MailMessage m = new MailMessage();
            SmtpClient sc = new SmtpClient();
            m.From = new MailAddress(fromAddress, "Home Page");
            m.To.Add("Admin@ramixpharma.com");
            m.Subject = subject;
            m.Body = body;
            sc.Host = "mail.ramixpharma.com";
            string str1 = "gmail.com";
            string str2 = fromAddress.ToLower();
            if (str2.Contains(str1))
            {
                try
                {
                    sc.Port = 587;
                    //sc.UseDefaultCredentials = false;
                    sc.Credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);
                    sc.EnableSsl = true;
                    sc.Send(m);
                    return;
                    //Response.Write("Email Send successfully");
                }
                catch (Exception ex)
                {
                    //Response.Write("<BR><BR>* Please double check the From Address and Password to confirm that both of them are correct. <br>");
                    //Response.Write("<BR><BR>If you are using gmail smtp to send email for the first time, please refer to this KB to setup your gmail account: http://www.smarterasp.net/support/kb/a1546/send-email-from-gmail-with-smtp-authentication-but-got-5_5_1-authentication-required-error.aspx?KBSearchID=137388");
                    //Response.End();
                    throw ex;
                }
            }
            else
            {
                try
                {
                    sc.Port = 8889;
                    //sc.UseDefaultCredentials = false;
                    sc.Credentials = new System.Net.NetworkCredential(fromAddress, fromPassword);
                    sc.EnableSsl = false;
                    sc.Send(m);
                    return;
                }
                catch (Exception ex)
                {
                    //Response.Write("<BR><BR>* Please double check the From Address and Password to confirm that both of them are correct. <br>");
                    //Response.End();
                    throw ex;
                }
            }


        }

    }
}

