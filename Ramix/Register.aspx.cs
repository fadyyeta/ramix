﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Register : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                wrong.Visible = false;
                //correctReg.Visible = false;
            }
        }
        protected void RegisterCheck()
        {
            if (!chkAgree.Checked)
            {
                return;
            }
            else
            {
                string fName = txtFName.Text,
                   lName = txtLName.Text,
                   email = txtEmail.Text,
                   pass = txtPass.Text;
                DataSet ds = codeClass.SQLREAD(" select * from [User] where emailAdd = N'" + email + "' ");
                if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
                {
                    int register = codeClass.inserGen("[User]", "firstName,lastName,emailAdd,passWors,active,accessLevel", "N'" + fName + "', N'" + lName + "', N'" + email + "', N'" + pass + "', N'No', N'user'");
                    if (register > 0)
                    {
                        // successful register
                        Session["userMail"] = email;
                        Session["userID"] = register;
                        Session["accLevel"] = "user";
                        Session["active"] = "No";
                        Session.Timeout = 1440;
                        Response.Write("<script language='javascript'>function redirectMe(){window.location='Admin.aspx';} function successReg() { $('#correctReg').css('display','block'); setTimeout(function() {redirectMe();}, 10000)}</script>");
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "successReg", "successReg()", true);
                    }
                }
                else
                {
                    wrong.Visible = true;
                }
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            RegisterCheck();
        }
    }
}