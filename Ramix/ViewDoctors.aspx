﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewDoctors.aspx.cs" Inherits="Ramix.ViewDoctors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Doctors
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
       /* .table {
            word-break: break-word;
        }

        #docTable thead tr th::after, #docTable thead tr th::before {
            display: none;
        }

        body .dataTables_wrapper .row:nth-child(1) {
            width: 70vw;
            margin-left: 2%;
        }

        body .dataTables_wrapper .row:nth-child(2) {
            overflow: auto;
            margin: 25px;
        }

        th.text-center {
            width: 30%;
            min-width: 200px;
            font-size: 0.8rem;
        }

        .dataTables_wrapper.dt-bootstrap4 {
            width: 100%;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <iframe id="txtArea1" style="display: none"></iframe>
    <button id="btnExport" class="btn btn-primary" onclick="tableToExcel('docTable','Doctors');">EXPORT </button>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Doctors</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <table id="docTable" class="table table-striped">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center">Doctor</th>
                                    <th class="text-center">Specialization</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:ListView ID="gvTest" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th class="text-center"><%# Eval("docName") %></th>
                                            <th class="text-center"><%# Eval("specialization") %></th>
                                            <th class="text-center"><%# Eval("category") %></th>
                                            <%--<th class="text-center"><%# Eval("address") %></th>--%>
                                            <th class="text-center"><%# Eval("area") %></th>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {
            $('#docTable').DataTable({
                ordering: true,
                //responsive: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });
    </script>
    <script src="assets/js/exportTable.js" charset="utf-8" type="text/javascript"></script>

</asp:Content>
