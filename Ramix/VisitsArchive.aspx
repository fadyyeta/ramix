﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeFile="VisitsArchive.aspx.cs" Inherits="Ramix.VisitsArchive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Visits
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <iframe id="txtArea1" style="display: none"></iframe>
    <asp:Button ID="btnExport" CssClass="btn btn-primary" runat="server" ClientIDMode="Static" OnClick="ExportBtnClick" OnClientClick="tableToExcel('visitTable','Visits');" Text="EXPORT" />
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Years</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <table id="years2Archive" class="table table-striped">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center">Year</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:ListView ID="gvYearArchive" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "year") %></th>
                                            <th class="text-center"><asp:Button CssClass="btn btn-info" ID="btnLoadData" CommandName="OnButtonClick" CommandArgument=<%# DataBinder.Eval(Container.DataItem, "year") %> runat="server" OnClick=<%# "OnButtonClick(" + DataBinder.Eval(Container.DataItem, "year") + ")" %> Text="Load Data" /></th>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
    <div class="col-md-12 ">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Visits Archive</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <table id="visitTable" class="table table-striped">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center ohHide">ID</th>
                                    <th class="text-center">Shift</th>
                                    <th class="text-center">Doctor</th>
                                    <th class="text-center">Specialization</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Doctor Comment</th>
                                    <th class="text-center">Pharmacy</th>
                                    <th class="text-center">Pharmacy Comment</th>
                                    <th class="text-center">date</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Location</th>
                                    <th class="text-center">Medical Rep</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:ListView ID="gvTest" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th class="text-center ohHide"><%# DataBinder.Eval(Container.DataItem, "ID") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "shift") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "docName") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "specialization") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "category") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "docComment") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "pharmacies") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "pharmComment") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "date", "{0:dd MMMM, yyyy hh:mm tt}") %></th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "address") %></th>
                                            <th class="text-center">
                                                <a target="_blank" href='https://www.google.com/maps/search/?api=1&query=<%# DataBinder.Eval(Container.DataItem, "latLong") %>'><%# DataBinder.Eval(Container.DataItem, "location") %></a>
                                            </th>
                                            <th class="text-center"><%# DataBinder.Eval(Container.DataItem, "fullName") %></th>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {
            $('#visitTable').DataTable({
                "deferRender": true,
                "deferLoading": 0,
                ordering: true,
                columnDefs: [{
                    "targets": 8,
                    "type": "date"
                }],
                order: [[8, 'desc']],
                lengthMenu: [[-1], ["All"]]
            });
        });
    </script>
    <script src="assets/js/exportTable.js" charset="utf-8" type="text/javascript"></script>

</asp:Content>
