﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewSales.aspx.cs" Inherits="Ramix.ViewSales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Sales
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        /*.table {*/
            /*word-break: break-word;*/
            /*width: 100%;
        }

        .dataTables_wrapper {
            width: 100%;
            padding: 5%;
        }

        .dataTables_wrapper .row:nth-child(2) {
            overflow: auto;*/
            /*width: 100%;*/
        /*}

        #salesTable thead tr th::after, #salesTable thead tr th::before {
            display: none;
        }

        th.text-center {*/
            /*width: 6.67%;*/
        /*}

        #salesTable {
            font-size: 1rem;
            width: 100%;
        }*/

        .img-thumbnail {
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <iframe id="txtArea1" style="display: none"></iframe>
    <button id="btnExport" class="btn btn-primary" onclick="tableToExcel('salesTable','Sales');">EXPORT </button>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Sales</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <table id="salesTable" class="table table-striped">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center ohHide">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Rep</th>
                                    <th class="text-center">pharmacy</th>
                                    <th class="text-center">site</th>
                                    <th class="text-center">area</th>
                                    <th class="text-center">Units</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">discount</th>
                                    <th class="text-center">bonus</th>
                                    <th class="text-center">bill</th>
                                    <th class="text-center">prod</th>
                                    <th class="text-center">paid</th>
                                    <th class="text-center">Invoice</th>
                                    <th class="text-center">date</th>
                                    <th class="text-center">total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:ListView ID="gvTest" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th class="text-center ohHide"><%# Eval("ID") %></th>
                                            <th class="text-center"><%# Eval("invoiceName") %></th>
                                            <th class="text-center"><%# Eval("UserName") %></th>
                                            <th class="text-center"><%# Eval("pharmacyName") %></th>
                                            <th class="text-center"><%# Eval("site") %></th>
                                            <th class="text-center"><%# Eval("area") %></th>
                                            <th class="text-center"><%# Eval("numberOfUnitsSold") %></th>
                                            <th class="text-center"><%# Eval("packagePrice") %></th>
                                            <th class="text-center"><%# Eval("discount") %></th>
                                            <th class="text-center"><%# Eval("bonus") %></th>
                                            <th class="text-center"><%# Eval("billNumber") %></th>
                                            <th class="text-center"><%# Eval("name") %></th>
                                            <th class="text-center"><%# Eval("paid") %></th>
                                            <th class="text-center">
                                                <a target="popup" onclick="window.open('<%# Eval("ImageURL") %>','popup','width=600,height=600'); return false;" class="link zoom" href='<%# Eval("ImageURL") %>'>
                                                    <img src='<%# Eval("ImageURL") %>' class="img img-rounded img-thumbnail" />
                                                </a>
                                            </th>
                                            <th class="text-center"><%# Eval("dateOrder", "{0:dd MMM, yyyy}") %></th>
                                            <th class="text-center"><%# Eval("total") %></th>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {
            $('#salesTable').DataTable({
                ordering: true,
                //responsive: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });
    </script>
    <script src="assets/js/exportTable.js" charset="utf-8" type="text/javascript"></script>

</asp:Content>
