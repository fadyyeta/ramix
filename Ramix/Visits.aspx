﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Visits.aspx.cs" Inherits="Ramix.Visits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Visits
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        .myForm.form-horizontal {
            margin: 0 20px;
        }

        .myForm label.col-form-label {
            text-align: left !important;
        }

        .dropdown.bootstrap-select {
            width: 100% !important;
        }

        .overlayAlert {
            background-color: rgba(0,0,0,0.5);
            width: -webkit-fill-available;
            height: -webkit-fill-available;
            vertical-align: middle;
            text-align: center;
            position: fixed;
            left: 0;
            z-index: 1111;
            top: 0;
            display: none;
        }

        .alert.alert-success {
            padding: 30px;
        }

        .btn-close {
            float: right;
            background: none;
            border: none;
            color: white;
            cursor: pointer;
            top: 5px;
            position: absolute;
            right: 5px;
        }

        .alert.alert-dismissible {
            position: relative;
            top: 30%;
            margin: auto;
            width: 60%;
            min-height: 400px;
            text-align: center;
            vertical-align: middle;
            font-size: 20pt;
            padding: 10%;
        }

            .alert.alert-dismissible i {
                color: white;
            }

            .alert.alert-dismissible span {
                line-height: 25pt;
            }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="overlayAlert" runat="server" id="overlayAlert">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fa fa-check"></i>
            <br />
            <span><strong>Visit is successfully created!</strong></span>
            <br />
            <span>close to add a new one.</span>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">X</button>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Visits</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Shift Select</label>
                        <div class="col-sm-10">
                            <asp:DropDownList ID="ddlShift" CssClass="form-control" ClientIDMode="Static" runat="server">
                                <asp:ListItem Enabled="false" Text="Please Choose" />
                                <asp:ListItem Text="AM" />
                                <asp:ListItem Text="PM" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Doctor's Name</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="txtDocName" placeholder="Doctor's Name" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Specialization Select</label>
                        <div class="col-sm-10">
                            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlSpeci_SelectedIndexChanged" ID="ddlSpeci" CssClass="form-control" ClientIDMode="Static" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rqDDLSpeci" ControlToValidate="ddlSpeci" runat="server" InitialValue="0" ValidationGroup="valVisitGroup" ErrorMessage="Please Choose a specialization" CssClass="alert alert-warning"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row" runat="server" id="otherSpec" visible="false">
                        <label class="col-sm-2 col-form-label">Specialization</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="txtOthSpec" placeholder="Specialization" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row ohHide">
                        <label class="col-sm-2 col-form-label">Specialization</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="txtDocSpec" placeholder="Specialization" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Category</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="txtDocCat" placeholder="Category" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Doctor Comments</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group txtArea">
                                <label class="bmd-label-floating">Comments about doctor from Rep.</label>
                                <asp:TextBox runat="server" Rows="5" TextMode="MultiLine" CssClass="form-control" ID="txtDocComment" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Pharmacies</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="txtPharmacies" placeholder="Pharmacies" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Pharmacy's Comments</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group txtArea">
                                <label class="bmd-label-floating">Comments about doctor from Pharmacies</label>
                                <asp:TextBox runat="server" Rows="5" TextMode="MultiLine" CssClass="form-control" ID="txtPharmComment" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="txtAddress" placeholder="Address" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Area</label>
                        <div class="col-sm-10">
                            <asp:DropDownList ID="ddlArea" CssClass="form-control" ClientIDMode="Static" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rqDDLArea" ControlToValidate="ddlArea" runat="server" InitialValue="0" ValidationGroup="valVisitGroup" ErrorMessage="Please Choose an area" CssClass="alert alert-warning"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <asp:Button Text="Submit" ID="btnSubmit" ClientIDMode="Static" ValidationGroup="valVisitGroup" OnClick="btnSubmit_Click" CssClass="btn btn-fill btn-rose" runat="server" />
            </div>
            <div class="ohHide">
                <asp:TextBox ID="lat" runat="server" ClientIDMode="Static" />
                <asp:TextBox ID="lon" runat="server" ClientIDMode="Static" />
                <div id="latlong">
                    <asp:TextBox runat="server" ID="locationJS" ClientIDMode="Static" />
                    <asp:TextBox runat="server" ID="latLongOnly" ClientIDMode="Static" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {
            getLocation();
        });
        $(".btn-close").click(function () {
            $(".overlayAlert").css("display", "none");
            window.location.replace("Visits.aspx");
        });
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                alert("Geolocation is not supported by this browser.");
            }
        }

        function showPosition(position) {
            $("#lat").val(position.coords.latitude);
            $("#lon").val(position.coords.longitude);
            var lat = $("#lat").val();
            var lon = $("#lon").val();
            var myLatLong = lat + "," + lon;
            $("#latLongOnly").val(myLatLong);
            //https://api.geoapify.com/v1/geocode/reverse?lat=39.78321267821705&lon=-105.00732421875001&apiKey=3ba9c4f3023b4e30989681460090c964
            //https://api.tomtom.com/search/2/reverseGeocode/27.7373868,30.8410112.json?key=mqulZ5FG76zvAZDt5UMbe3tbpnj32PFx&radius=10
            //"https://www.mapquestapi.com/geocoding/v1/reverse?key=r8lrqzusScNAW1YQYG1awqGvG4ojr7T1&location=" + lat + "," + lon + "&includeRoadMetadata=true&includeNearestIntersection=true"
            $.ajax({
                //url: "https://api.geoapify.com/v1/geocode/reverse?lat=" + lat + "&lon=" + lon + "&apiKey=3ba9c4f3023b4e30989681460090c964",
                url: "https://api.tomtom.com/search/2/reverseGeocode/" + lat + "," + lon + ".json?key=mqulZ5FG76zvAZDt5UMbe3tbpnj32PFx&radius=100",
                success: function (result) {
                    //var fullAdd = result.features[0].properties;
                    //var fullAdd = result.results[0].locations[0];
                    //$("#locationJS").val(fullAdd.street + ", " + fullAdd.adminArea5 + " " + fullAdd.adminArea4 + " " + fullAdd.adminArea3 + ", " + fullAdd.postalCode);
                    var fullAdd = result.addresses[0];
                    $("#locationJS").val(fullAdd.address.freeformAddress);
                }
            });
        }
    </script>

</asp:Content>
