﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="approveUsers.aspx.cs" Inherits="Ramix.approveUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Approve Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Approve Users</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Users Select</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlUsers" ClientIDMode="static">
                                <asp:ListItem Text="Please Choose" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <asp:Button CssClass="btn btn-fill btn-rose" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Approve" runat="server" />
            </div>
        </div>
    </div>
    <br />
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Manage Users</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Users Select</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlActiveUsers" ClientIDMode="static">
                                <asp:ListItem Text="Please Choose" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <asp:Button CssClass="btn btn-fill btn-danger" ID="btnDeactivate" OnClick="btnDeactivate_Click" Text="Block" runat="server" />
                <asp:Button CssClass="btn btn-fill btn-success" ID="btnPromoteAdmin" OnClick="btnPromoteAdmin_Click"  Text="Make Area Manager" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
