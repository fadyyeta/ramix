﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //forAreaManager.Visible = false;
            if (Session["userID"] == null || Session["active"].ToString() == "No")
            {
                Response.Redirect("login.aspx");
            }
            if (Session["accLevel"].ToString() != "Admin")
            {
                forAdminsOnly.Visible = false;
            }
            if (Session["accLevel"].ToString() == "AreaManager")
            {
                forAreaManager.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}