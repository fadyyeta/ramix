﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class approveUsers : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["accLevel"].ToString() != "Admin")
                {
                    btnPromoteAdmin.Visible = false;
                }
                loadUsers();
                loadActiveUsers();
            }
        }

        private void loadUsers()
        {
            DataSet ds = codeClass.SQLREAD("select * from [User] where active = N'No'");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string fName = ds.Tables[0].Rows[i]["firstName"].ToString();
                string lName = ds.Tables[0].Rows[i]["lastName"].ToString();
                string uMail = ds.Tables[0].Rows[i]["emailAdd"].ToString();
                int uID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString());
                ListItem li = new ListItem(fName + " " + lName + " - " + uMail, uID.ToString());
                ddlUsers.Items.Add(li);
            }
        }
        private void loadActiveUsers()
        {
            DataSet ds = codeClass.SQLREAD("select * from [User] where active = N'Yes'");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string fName = ds.Tables[0].Rows[i]["firstName"].ToString();
                string lName = ds.Tables[0].Rows[i]["lastName"].ToString();
                string uMail = ds.Tables[0].Rows[i]["emailAdd"].ToString();
                int uID = int.Parse(ds.Tables[0].Rows[i]["ID"].ToString());
                ListItem li = new ListItem(fName + " " + lName + " - " + uMail, uID.ToString());
                ddlActiveUsers.Items.Add(li);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int uID = int.Parse(ddlUsers.SelectedValue);
            string txt = "active = N'Yes' ";
            codeClass.updateGen("[User]", uID, txt);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            int uID = int.Parse(ddlActiveUsers.SelectedValue);
            string txt = "active = N'No' ";
            codeClass.updateGen("[User]", uID, txt);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void btnPromoteAdmin_Click(object sender, EventArgs e)
        {
            int uID = int.Parse(ddlActiveUsers.SelectedValue);
            string txt = "accessLevel = N'AreaManager' ";
            codeClass.updateGen("[User]", uID, txt);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }
}