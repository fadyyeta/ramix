﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Sales : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadProducts();
                loadArea();
            }
        }
        public void loadArea()
        {
            ListItem liInit = new ListItem("Please Choose Area", "0");
            liInit.Attributes.Add("disabled", "disabled");
            liInit.Selected = true;
            ddlArea.Items.Add(liInit);
            DataSet ds = codeClass.SQLREAD("select * from Areas where id != 21 order by name asc");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ListItem li = new ListItem(ds.Tables[0].Rows[i]["name"].ToString(), ds.Tables[0].Rows[i]["ID"].ToString());
                ddlArea.Items.Add(li);
            }
            ListItem liOth = new ListItem("غير معروف", "21");
            ddlArea.Items.Add(liOth);
        }
        public void loadProducts()
        {
            ListItem liInit = new ListItem("Please Choose Product", "0");
            liInit.Attributes.Add("disabled", "disabled");
            liInit.Selected = true;
            ddlProduct.Items.Add(liInit);
            DataSet ds = codeClass.SQLREAD("SELECT * FROM Product WHERE ID > 1");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ListItem li = new ListItem(ds.Tables[0].Rows[i]["name"].ToString(), ds.Tables[0].Rows[i]["ID"].ToString());
                ddlProduct.Items.Add(li);
            }
            //lvProduct.DataSource = ds;
            //lvProduct.DataBind();

            //lvProductNoValue.DataSource = ds;
            //lvProductNoValue.DataBind();
        }
        public void TraverseControlsAndSetTextEmpty(Control control)
        {
            foreach (Control c in control.Controls)
            {
                var box = c as TextBox;
                if (box != null)
                {
                    box.Text = string.Empty;
                }

                this.TraverseControlsAndSetTextEmpty(c);
            }
        }
        protected DateTime GetCurrentTime()
        {
            DateTime serverTime = DateTime.Now;
            DateTime _localTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(serverTime, TimeZoneInfo.Local.Id, "Egypt Standard Time");
            return _localTime;
        }
        protected string UploadThisFile(FileUpload file)
        {
            if (file.HasFile && file.PostedFile.ContentLength > 0)
            {
                //string theFileName = Path.Combine(Server.MapPath("~/img/invoice"), file.FileName);
                //if (File.Exists(theFileName))
                //{
                //    File.Delete(theFileName);
                //}
                //file.SaveAs(theFileName);
                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                string ImagePath = "/img/invoice/" + filename + Path.GetExtension(file.FileName);

                file.SaveAs(Server.MapPath(ImagePath));
                return ImagePath;
            }
            return "not provided";
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            string ImgURL = UploadThisFile(imgURL);
            string Pharmacy = pharmName.Text;
            string Site = pharmSite.Text;
            int Area = int.Parse(ddlArea.SelectedValue);
            int Units = int.Parse(numSold.Text);
            decimal Price = decimal.Parse(pkgPrice.Text);
            decimal discount = decimal.Parse(Discount.Text);
            decimal Bonus = decimal.Parse(bonus.Text);
            string BillNum = billNum.Text;
            //string Product = product.Text;
            string Product = ddlProduct.SelectedValue;
            decimal PaidAmount = decimal.Parse(payment.Text);
            string InvoiceName = BillNum + " - " + Pharmacy;
            string dateOrder = GetCurrentTime().ToShortDateString();
            decimal total = Units * (Price * ((100 - discount) / 100));
            string param = "N'" + InvoiceName + "', N'" + Pharmacy + "', N'" + Site + "', " + Area + ", " + Units + ", " + Price + ", " + discount + ", " + Bonus + ", N'" + BillNum + "', " + Product + ", " + PaidAmount + ", N'" + dateOrder + "', " + total + ", " + Session["userID"] + ", N'" + ImgURL + "' ";
            string cols = "invoiceName ,pharmacyName ,site ,area ,numberOfUnitsSold ,packagePrice ,discount ,bonus ,billNumber ,productID ,paid ,dateOrder ,total ,userID, ImageURL";
            int res = codeClass.inserGen("Invoice", cols, param);
            TraverseControlsAndSetTextEmpty(this);
        }

        protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedVal = ddlProduct.SelectedValue.ToString();
            DataSet ds = codeClass.SQLREAD("SELECT * FROM Product WHERE ID = " + selectedVal);
            string price = ds.Tables[0].Rows[0]["price"].ToString();
            if (price != null)
            {
                pkgPrice.Text = price;
            }
        }
    }
}