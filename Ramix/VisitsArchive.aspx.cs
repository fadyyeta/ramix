﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class VisitsArchive : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadYears();

            }
        }
        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            gvYearArchive.ItemCommand += new EventHandler<ListViewCommandEventArgs>(OnButtonClick);
        }
        protected void loadYears()
        {
            int startYear = 2020;
            int thisYear = int.Parse(DateTime.Now.ToString("yyyy"));
            int loopLength = thisYear - startYear;
            DataTable dt = new DataTable();
            dt.Columns.Add("year");
            for (int i = 0; i <= loopLength; i++)
            {
                //Console.WriteLine(startYear + i);
                DataRow dr = dt.NewRow();
                dr["year"] = startYear + i;
                dt.Rows.Add(dr);
            }
            gvYearArchive.DataSource = dt;
            gvYearArchive.DataBind();
        }

        public void loadVisit(int year)
        {
            DataSet ds = codeClass.SQLREAD("SELECT Visit.ID,Visit.shift,Visit.docName,speci.name as specialization,Visit.category,Visit.docComment,Visit.pharmacies,Visit.pharmComment,Visit.date,Visit.address,Visit.location,Visit.latLong,[User].fullName FROM [User] INNER JOIN Visit ON [User].ID = Visit.userID  inner join Speci on Speci.ID = Visit.speci where date between '" + year + "/01/01' AND '" + (year+1) + "/01/01'  order by date desc");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }

        protected void OnButtonClick(object sender, CommandEventArgs e)
        {
            int year = int.Parse(e.CommandArgument.ToString());
            loadVisit(year);
            ExportBtnClick(sender, e);
        }
        protected void ExportBtnClick(object sender, System.EventArgs e)
        {
            
        }

    }
}
