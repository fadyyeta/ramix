﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
namespace Ramix
{
    public class CodeClass
    {
        //Data Access Layer
        public DataSet SQLREAD(string SQLQUERY)
        {
            DataSet functionReturnValue = null;
            try
            {
                SqlConnection thisConnection = null;
                thisConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Connstr"].ToString());
                string sql = SQLQUERY;
                thisConnection.Open();
                DataSet DS = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(sql, thisConnection);
                da.Fill(DS);
                functionReturnValue = DS;
                thisConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return functionReturnValue;
        }
        public string SQLINSERT(string SQLQUERY)
        {
            string functionReturnValue = null;
            DataSet dsID = new DataSet();
            try
            {
                SqlConnection thisConnection = null;
                thisConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Connstr"].ToString());
                string sql = SQLQUERY;

                thisConnection.Open();

                dynamic objCmd = null;
                objCmd = new System.Data.SqlClient.SqlCommand();
                var _with1 = objCmd;
                _with1.Connection = thisConnection;
                _with1.CommandType = CommandType.Text;
                _with1.CommandText = sql;
                objCmd.ExecuteNonQuery();


                functionReturnValue = "Data insertion Successful !";
                thisConnection.Close();

            }
            catch (Exception ex)
            {
                //  Lookups.LogErrorToText("Web", "Settings.vb", "FillMenus", ex.Message)

            }
            return functionReturnValue;
        }
        public int SQLGetLastID(string SQLQUERY)
        {
            try
            {
                SqlConnection thisConnection = null;
                thisConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Connstr"].ToString());
                string sql = SQLQUERY;

                thisConnection.Open();

                SqlDataAdapter da = new SqlDataAdapter(sql, thisConnection);
                DataSet dsID = new DataSet();

                // Fill the DataSet using default values for DataTable names, etc
                da.Fill(dsID);

                thisConnection.Close();
                return Convert.ToInt32(dsID.Tables[0].Rows[0][0].ToString());

            }
            catch (Exception ex)
            {
                return 0;

            }


        }
        public int inserGen(string tblName, string cols, string txt)
        {
            try
            {
                DataSet dsNew = SQLREAD("INSERT INTO " + tblName + " (" + cols + ") VALUES (" + txt + ") SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]");
                int id = int.Parse(dsNew.Tables[0].Rows[0][0].ToString());
                return id;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }
        public void updateGen(string tblName, int idUp, string txt)
        {
            try
            {
                //email = '"+Email+"',fName = '"+Fname+ "',lName = '"+Lname+"',address = '"+Address+ "',city = '"+City+ "',about = '"+About+"'
                string queryUp = "Update " + tblName + " SET " + txt + " Where ID = " + idUp;
                SQLINSERT(queryUp);
            }
            catch (Exception ex)
            {

            }
        }
    }
}