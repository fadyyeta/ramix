﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class ViewPharmacies : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadPharmacies();
            }
        }
        protected void loadPharmacies()
        {
            DataSet ds = codeClass.SQLREAD("(select distinct pharmacies,address from Visit where pharmacies <> '')  union (select pharmacyName, Areas.name as area from invoice inner join areas on invoice.area = Areas.ID )  order by pharmacies asc");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }
    }

}