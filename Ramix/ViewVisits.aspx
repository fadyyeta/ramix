﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewVisits.aspx.cs" Inherits="Ramix.ViewVisits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Visits
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        /*.table {
            word-break: break-word;
        }

        #visitTable thead tr th::after, #visitTable thead tr th::before {
            display: none;
        }

        .myForm {*/
        /*overflow: auto;*/
        /*}

        body .dataTables_wrapper .row:nth-child(1) {
            width: 70vw;
            margin-left: 2%;
        }

        body .dataTables_wrapper .row:nth-child(2) {
            overflow: auto;
            margin: 25px;
        }

        th.text-center {
            width: 30%;
            min-width: 200px;
            font-size: 0.8rem;
        }

        thead th.text-center {
            font-size: 1.2rem !important;
        }

        #visitTable {
            width: 100% !important;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <iframe id="txtArea1" style="display: none"></iframe>
    <button id="btnExport" class="btn btn-primary" onclick="tableToExcel('visitTable','Visits');">EXPORT </button>
    <button id="btnLoadFull" class="btn btn-secondary" runat="server" onserverclick="btnLoadFull_ServerClick">Load Full Visits </button>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Visits</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <table id="visitTable" class="table table-striped">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center ohHide">ID</th>
                                    <th class="text-center">Shift</th>
                                    <th class="text-center">Doctor</th>
                                    <th class="text-center">Specialization</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Doctor Comment</th>
                                    <th class="text-center">Pharmacy</th>
                                    <th class="text-center">Pharmacy Comment</th>
                                    <th class="text-center">date</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Location</th>
                                    <th class="text-center">Medical Rep</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:ListView ID="gvTest" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th class="text-center ohHide"><%# Eval("ID") %></th>
                                            <th class="text-center"><%# Eval("shift") %></th>
                                            <th class="text-center"><%# Eval("docName") %></th>
                                            <th class="text-center"><%# Eval("specialization") %></th>
                                            <th class="text-center"><%# Eval("category") %></th>
                                            <th class="text-center"><%# Eval("docComment") %></th>
                                            <th class="text-center"><%# Eval("pharmacies") %></th>
                                            <th class="text-center"><%# Eval("pharmComment") %></th>
                                            <th class="text-center"><%# Eval("date", "{0:dd MMMM, yyyy hh:mm tt}") %></th>
                                            <th class="text-center"><%# Eval("address") %></th>
                                            <th class="text-center">
                                                <a target="_blank" href='https://www.google.com/maps/search/?api=1&query=<%# Eval("latLong") %>'><%# Eval("location") %></a>
                                            </th>
                                            <th class="text-center"><%# Eval("fullName") %></th>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {
            $('#visitTable').DataTable({
                //"processing": true,
                //"serverSide": true,
                "deferRender": true,
                "deferLoading": 10,
                ordering: true,
                columnDefs: [{
                    "targets": 8,
                    "type": "date"
                }],
                order: [[8, 'desc']],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });
    </script>
    <script src="assets/js/exportTable.js" charset="utf-8" type="text/javascript"></script>

</asp:Content>
