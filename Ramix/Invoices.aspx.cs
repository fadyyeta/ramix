﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Invoices : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadInvoices();
            }
        }
        public void loadInvoices()
        {
            DataSet ds = codeClass.SQLREAD("SELECT * FROM Invoice where paid < total and userID = "+ Session["userID"]);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                double paid = double.Parse(ds.Tables[0].Rows[i]["paid"].ToString());
                double total = double.Parse(ds.Tables[0].Rows[i]["total"].ToString());
                ListItem li = new ListItem(ds.Tables[0].Rows[i]["invoiceName"].ToString() + " - remaining : " + (total - paid).ToString(), ds.Tables[0].Rows[i]["ID"].ToString());
                ddlInvoice.Items.Add(li);
            }
        }
        public void TraverseControlsAndSetTextEmpty(Control control)
        {
            foreach (Control c in control.Controls)
            {
                var box = c as TextBox;
                if (box != null)
                {
                    box.Text = string.Empty;
                }

                this.TraverseControlsAndSetTextEmpty(c);
            }
        }
        private decimal invoicePaidAmount(int invoiceID, decimal payment)
        {
            decimal toPay = 0;
            DataSet ds = codeClass.SQLREAD("select paid from invoice where id = " + invoiceID);
            decimal paid = decimal.Parse(ds.Tables[0].Rows[0]["paid"].ToString());
            toPay = paid + payment;
            return toPay;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int invoice = int.Parse(ddlInvoice.SelectedValue);
            decimal amount = decimal.Parse(paymentAmount.Text);
            DateTime PaymentDate = DateTime.Parse(paymentDate.Text);
            decimal updatedPayment = invoicePaidAmount(invoice,amount);
            string txt = "paid = "+ updatedPayment;

            codeClass.updateGen("Invoice", invoice, txt);
            TraverseControlsAndSetTextEmpty(this);
        }
    }
}