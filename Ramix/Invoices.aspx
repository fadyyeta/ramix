﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Invoices.aspx.cs" Inherits="Ramix.Invoices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Invoices
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Invoice</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Invoice Select</label>
                        <div class="col-sm-10">
                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlInvoice" ClientIDMode="static">
                                <asp:ListItem Text="Please Choose" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Amount</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control" ID="paymentAmount" ClientIDMode="Static" placeholder="Payment Amount" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Payment Date</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox CssClass="form-control datetimepicker" ClientIDMode="Static" id="paymentDate" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <asp:Button CssClass="btn btn-fill btn-rose" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script src="assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>
</asp:Content>
