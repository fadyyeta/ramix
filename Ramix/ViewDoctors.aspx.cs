﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class ViewDoctors : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadDoctors();
            }
        }
        protected void loadDoctors()
        {
            DataSet ds = codeClass.SQLREAD("select distinct docname,specialization,category,address, Areas.name as area from Visit inner join areas on visit.area = Areas.ID where docName <> N'' order by docName asc");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }
    }

}