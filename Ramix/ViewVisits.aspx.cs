﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class ViewVisits : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadVisit();
            }
        }
        protected void loadVisit()
        {
            DataSet ds = codeClass.SQLREAD("SELECT top 2000 Visit.ID,Visit.shift,Visit.docName,speci.name as specialization,Visit.category,Visit.docComment,Visit.pharmacies,Visit.pharmComment,Visit.date,Visit.address,Visit.location,Visit.latLong,[User].fullName FROM [User] INNER JOIN Visit ON [User].ID = Visit.userID  inner join Speci on Speci.ID = Visit.speci  order by date desc");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }
        protected void loadFullVisits()
        {
            DataSet ds = codeClass.SQLREAD("SELECT Visit.ID,Visit.shift,Visit.docName,speci.name as specialization,Visit.category,Visit.docComment,Visit.pharmacies,Visit.pharmComment,Visit.date,Visit.address,Visit.location,Visit.latLong,[User].fullName FROM [User] INNER JOIN Visit ON [User].ID = Visit.userID  inner join Speci on Speci.ID = Visit.speci  order by date desc");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }

        protected void btnLoadFull_ServerClick(object sender, EventArgs e)
        {
            loadFullVisits();
        }
    }

}