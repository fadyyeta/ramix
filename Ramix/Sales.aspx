﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Sales.aspx.cs" Inherits="Ramix.Sales" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Sales
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        .myForm.form-horizontal {
            margin: 0 20px;
        }

        .myForm label.col-form-label {
            text-align: left !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">

    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Sales Report</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Pharmacy Name</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pharmName" CssClass="form-control" placeholder="Pharmacy Name" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Site</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pharmSite" CssClass="form-control" placeholder="Site" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Area</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <%--<asp:TextBox runat="server" ID="pharmArea" CssClass="form-control" placeholder="Area" />--%>
                                <asp:DropDownList ID="ddlArea" CssClass="form-control" ClientIDMode="Static" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rqddlArea" ControlToValidate="ddlArea" runat="server" InitialValue="0" ValidationGroup="valSalesGroup" ErrorMessage="Please Choose an area" CssClass="alert alert-warning"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Number of units sold</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="numSold" CssClass="form-control" placeholder="Number of units sold" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Product</label>
                        <div class="col-sm-10">
                            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" runat="server" CssClass="form-control" ID="ddlProduct" ClientIDMode="static">
                                <%--                                <asp:ListItem Enabled="false" Selected="True" Text="Please Choose" />--%>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rqddlProduct" ControlToValidate="ddlProduct" runat="server" InitialValue="0" ValidationGroup="valSalesGroup" ErrorMessage="Please Choose a product" CssClass="alert alert-warning"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Package price</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pkgPrice" Enabled="false" CssClass="form-control" placeholder="Package price" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Discount</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="Discount" CssClass="form-control" placeholder="Discount" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Bonus</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="bonus" CssClass="form-control" placeholder="Bonus" />
                            </div>
                        </div>
                    </div>
                    <div class="row ohHide">
                        <label class="col-sm-2 col-form-label">Sample</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="Sample" CssClass="form-control" placeholder="Sample" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Bill Number</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="billNum" CssClass="form-control" placeholder="Bill Number" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Invoice Image</label>
                        <div class="col-sm-10">
                            <div class="form-group form-file-upload form-file-multiple">
                                <input type="file" multiple="" class="inputFileHidden">
                                <div class="input-group">
                                    <asp:FileUpload data-default-file="/img/invoice/357468.jpg" AllowMultiple="false" ClientIDMode="Static" class="" ID="imgURL" runat="server" />
                                    <input type="text" id="UploadURL" class="form-control inputFileVisible" placeholder="Invoice Image">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-fab btn-round btn-primary btnAttach">
                                            <i class="material-icons">attach_file</i>
                                        </button>
                                    </span>
                                    <asp:RequiredFieldValidator ID="rqimgURL" ControlToValidate="imgURL" runat="server" InitialValue="" ValidationGroup="valSalesGroup" ErrorMessage="Please Choose an image" CssClass="alert alert-warning"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label label-checkbox">Payment</label>
                        <div class="col-sm-10 checkbox-radios">
                            <%--<div class="form-check bmd-form-group">
                                form-check form-check-inline
                                <label class="form-check-label">
                                    <input class="form-check-input" name="exampleRadios" type="radio" value="Yes" checked>Yes<span class="circle"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="exampleRadios" type="radio" value="No">No<span class="circle"><span class="check"></span></span>
                                </label>
                            </div>--%>
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="payment" CssClass="form-control" placeholder="paid amount" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer ">
                <asp:Button ValidationGroup="valSalesGroup" Text="Submit" ID="btnSubmit" ClientIDMode="Static" OnClick="btnSubmit_ServerClick" CssClass="btn btn-fill btn-rose" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $("#UploadURL, .btnAttach").on("click", function () {
            $("#imgURL").click();
        });
        $("#imgURL").on("change", function (e) {
            let fileName = e.target.files[0].name;
            $("#UploadURL").val(fileName);
            //$("#UploadURL").blur();
        });
        $(function () {
            var dropDownList1 = $('select[id$=ddlProduct]');
            dropDownList1.removeAttr('onchange');
            dropDownList1.change(function (e) {
                if (this.value != 0) {
                    setTimeout('__doPostBack(\'ddlProduct\',\'\')', 0);
                }
            });
        });
        $(function () {
            var dropDownList2 = $('select[id$=ddlArea]');
            dropDownList2.removeAttr('onchange');
            dropDownList2.change(function (e) {
                if (this.value != 0) {
                    setTimeout('__doPostBack(\'ddlArea\',\'\')', 0);
                }
            });
        });
    </script>
</asp:Content>
