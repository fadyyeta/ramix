﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Login : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                wrong.Visible = false;
                Session.Abandon();
            }
        }
        protected void LoginCheck()
        {
            string fName = txtFName.Text,
                   email = txtEmail.Text,
                   pass = txtPass.Text;
            DataSet ds = codeClass.SQLREAD(" select * from [User] where emailAdd = N'" + email + "' and passWors = N'" + pass + "' and active = N'Yes'");
            if (ds == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {
                wrong.Visible = true;
            }
            else
            {
                // successful login
                Session["userMail"] = ds.Tables[0].Rows[0]["emailAdd"].ToString();
                Session["userID"] = ds.Tables[0].Rows[0]["ID"].ToString();
                Session["accLevel"] = ds.Tables[0].Rows[0]["accessLevel"].ToString();
                Session["active"] = ds.Tables[0].Rows[0]["active"].ToString();
                Session.Timeout = 1440;
                Response.Redirect("Admin.aspx");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            LoginCheck();
        }
    }
}