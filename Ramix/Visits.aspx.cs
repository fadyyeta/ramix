﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;

namespace Ramix
{
    public partial class Visits : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        public int getSpeci()
        {
            int speciID;
            string selectedVal = ddlSpeci.SelectedValue.ToString();
            if (selectedVal == "Other")
            {
                string newSpec = txtOthSpec.Text;
                speciID = codeClass.inserGen("Speci", "name", "N'" + newSpec + "'");
            }
            else
            {
                speciID = int.Parse(selectedVal);
            }
            return speciID;
        }
        public void loadArea()
        {
            ListItem liInit = new ListItem("Please Choose Area", "0");
            liInit.Attributes.Add("disabled", "disabled");
            liInit.Selected = true;
            ddlArea.Items.Add(liInit);
            DataSet ds = codeClass.SQLREAD("select * from Areas where id != 21 order by name asc");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ListItem li = new ListItem(ds.Tables[0].Rows[i]["name"].ToString(), ds.Tables[0].Rows[i]["ID"].ToString());
                ddlArea.Items.Add(li);
            }
            ListItem liOth = new ListItem("غير معروف", "21");
            ddlArea.Items.Add(liOth);
        }

        public void loadSpeci()
        {
            ListItem liInit = new ListItem("Please Choose Specialization", "0");
            liInit.Attributes.Add("disabled", "disabled");
            liInit.Selected = true;
            ddlSpeci.Items.Add(liInit);
            DataSet ds = codeClass.SQLREAD("SELECT distinct * FROM Speci where name != ''");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ListItem li = new ListItem(ds.Tables[0].Rows[i]["name"].ToString(), ds.Tables[0].Rows[i]["ID"].ToString());
                ddlSpeci.Items.Add(li);
            }
            ListItem liOth = new ListItem("Other", "Other");
            ddlSpeci.Items.Add(liOth);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadSpeci();
                loadArea();
                //overlayAlert.Visible = false;
            }
        }
        public void TraverseControlsAndSetTextEmpty(Control control)
        {
            foreach (Control c in control.Controls)
            {
                var box = c as TextBox;
                if (box != null)
                {
                    box.Text = string.Empty;
                }

                this.TraverseControlsAndSetTextEmpty(c);
            }
        }
        protected DateTime GetCurrentTime()
        {
            DateTime serverTime = DateTime.Now;
            DateTime _localTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(serverTime, TimeZoneInfo.Local.Id, "Egypt Standard Time");
            return _localTime;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string Shift = ddlShift.SelectedItem.Text;
            int Area = int.Parse(ddlArea.SelectedValue.ToString());
            string DocName = txtDocName.Text;
            int Specialization = getSpeci();
            string Category = txtDocCat.Text;
            string DocComment = txtDocComment.Text;
            string Pharmacy = txtPharmacies.Text;
            string PharmComment = txtPharmComment.Text;
            string dateVisit = GetCurrentTime().ToString();
            string Address = txtAddress.Text;
            string LatLong = latLongOnly.Text;
            string LatLong2 = lat.Text + "," + lon.Text;
            string location = locationJS.Text;
            string param = "N'" + Shift + "', N'" + DocName + "', " + Specialization + ", N'" + Category + "', N'" + DocComment + "', N'" + Pharmacy + "', N'" + PharmComment + "', N'" + dateVisit + "', N'" + Address + "', " + Session["userID"] + ", N'" + LatLong2 + "', N'" + location + "', " + Area;
            string cols = "shift, docName, speci, category, docComment, pharmacies, pharmComment, date, address, userID, [latLong], location, area ";
            int res = codeClass.inserGen("Visit", cols, param);
            TraverseControlsAndSetTextEmpty(this);
            overlayAlert.Attributes.CssStyle.Add("display", "inline-block");

        }

        protected void ddlSpeci_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedVal = ddlSpeci.SelectedValue.ToString();
            if (selectedVal == "Other")
            {
                otherSpec.Visible = true;
            }
            else
            {
                otherSpec.Visible = false;
            }
        }
    }
}