﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewPharmacies.aspx.cs" Inherits="Ramix.ViewPharmacies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Pharmacies
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        /*.table {
            word-break: break-word;
        }

        #pharmTable thead tr th::after, #pharmTable thead tr th::before {
            display: none;
        }

        th.text-center {*/
            /*width: 25%;*/
        /*}

        .dataTables_wrapper.dt-bootstrap4 {
            width: 100%;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <iframe id="txtArea1" style="display: none"></iframe>
    <button id="btnExport" class="btn btn-primary" onclick="tableToExcel('pharmTable','Pharmacies');">EXPORT </button>
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Pharmacies</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <table id="pharmTable" class="table table-striped">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center">pharmacies</th>
                                    <th class="text-center">Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:ListView ID="gvTest" ClientIDMode="Static" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th class="text-center"><%# Eval("pharmacies") %></th>
                                            <th class="text-center"><%# Eval("address") %></th>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {
            $('#pharmTable').DataTable({
                ordering: true,
                //responsive: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        });
    </script>
    <script src="assets/js/exportTable.js" charset="utf-8" type="text/javascript"></script>
</asp:Content>
