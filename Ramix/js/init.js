jQuery(document).ready(function(){

	"use strict";
	
	function renex_service_carousel(){
		var carousel			= jQuery('.renex_services_wrap .owl-carousel');
		carousel.owlCarousel({
				loop: true,
				items: 3,
				lazyLoad: true,
				margin: 40,
				autoplay: true,
				autoplayTimeout: 5500,
				smartSpeed: 2000,
				dots: true,
				nav: false,
				navSpeed: true,
				responsive:{
				0:{items:1},
				480:{items:1},
				768:{items:2},
				1040:{items:3},
				1200:{items:3},
				1600:{items:3},
				1920:{items:3}
			}
		});
		renex_modal_news();
		jQuery('.renex_services_wrap .service_list .custom_nav > a.prev').on('click', function(){
		carousel.trigger('prev.owl.carousel');
		return false;
	});
	
	jQuery('.renex_services_wrap .service_list .custom_nav > a.next').on('click', function(){
		carousel.trigger('next.owl.carousel');
		return false;
	});
		
		var carousel2			= jQuery('.renex_news .owl-carousel');
		carousel2.owlCarousel({
				loop: true,
				items: 3,
				lazyLoad: true,
				margin: 40,
				autoplay: true,
				autoplayTimeout: 5500,
				smartSpeed: 2000,
				dots: true,
				nav: false,
				navSpeed: true,
				responsive:{
				0:{items:1},
				480:{items:1},
				768:{items:2},
				1040:{items:3},
				1200:{items:3},
				1600:{items:3},
				1920:{items:3}
			}
		});
		
		jQuery('.renex_news .custom_nav > a.prev').on('click', function(){
		carousel2.trigger('prev.owl.carousel');
		return false;
	});
	
	jQuery('.renex_news .custom_nav > a.next').on('click', function(){
		carousel2.trigger('next.owl.carousel');
		return false;
	});
		
	}
	renex_service_carousel();
	
		
	/************************* SVG Image ****************************/
	
	
	function renex_svg(){
		
	jQuery('img.svg').each(function(){
		
		var jQueryimg 		= jQuery(this);
		var imgClass		= jQueryimg.attr('class');
		var imgURL			= jQueryimg.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var jQuerysvg = jQuery(data).find('svg');

			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				jQuerysvg = jQuerysvg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			jQuerysvg = jQuerysvg.removeAttr('xmlns:a');

			// Replace image with new SVG
			jQueryimg.replaceWith(jQuerysvg);

		}, 'xml');

	});
}
renex_svg();
	
	
	/************************* About Image ****************************/
	
	
	function renex_about_img(){
		
		var name		= jQuery('.renex_hero_wrap .about_main p .name,.renex_hero_wrap_video .about_main p .name');
		var image		= jQuery('.renex_hero_wrap .about_main .about_image,.renex_hero_wrap_video .about_main .about_image');
		
		name.on('mouseenter',function(){
			image.addClass('opened');
		})
		.on('mouseleave',function(){
			image.removeClass('opened');
		});
		
	}
	renex_about_img();
	
	
	/************************* Progress Bar ****************************/
	
	
	function tdProgress(container){
		
		container.find('.progress_inner').each(function(i) {
			var progress 		= jQuery(this);
			var pValue 			= parseInt(progress.data('value'), 10);
			var pColor			= progress.data('color');
			var pBarWrap 		= progress.find('.bar');
			var pBar 			= progress.find('.bar_in');
			pBar.css({width:pValue+'%', backgroundColor:pColor});
			setTimeout(function(){pBarWrap.addClass('open');},(i*300));
		});
	}

		jQuery('.renex_progress').each(function() {
			
			var pWrap 			= jQuery(this);
			pWrap.waypoint({handler: function(){tdProgress(pWrap);},offset:'90%'});	
		});
	
	
	/************************* Images ****************************/
	
	
	function renex_images(){
		
	var data			= jQuery('*[data-img-url]');
	
	data.each(function(){
		var element		= jQuery(this);
		var url			= element.data('img-url');
		element.css({backgroundImage: 'url('+url+')'});
	});
}
renex_images();
	
	
	/************************* Hero Height ****************************/
	
	
	function renex_hero_height(){
		
	var WH		= jQuery(window).height();
	var hero	= jQuery('.renex_hero_wrap');
		
	hero.css({height:WH});
}
renex_hero_height();	
	
	
	/************************* About Top ****************************/
	
	
	function renex_about_top(){
		
	var hero	= jQuery('.renex_hero_wrap').height();
	var about	= jQuery('#about');
		
	about.css({marginTop:hero});
}
renex_about_top();	
	
	
	/************************* Menu Background ****************************/
	
	
	function renex_menu_bg(){
	jQuery(window).on('scroll',function(){
		var WinOffset		= jQuery(window).scrollTop();
		var topBar			= jQuery('.renex_topbar');
		if(WinOffset >= 500){
			topBar.addClass('animate');
		}else{
			topBar.removeClass('animate');
		}
	});
}
renex_menu_bg();
	
	
	/************************* Mobile Menu ****************************/
	
	
	function renex_mobile_menu(){
		
	var trigger			= jQuery('.renex_topbar .trigger');
	var triggerMenu		= jQuery('.renex_topbar .trigger .menu');
	var triggerClose	= jQuery('.renex_topbar .trigger .close');
	var dropdown		= jQuery('.renex_topbar .dropdown');
	
	trigger.on('click',function(){
		var element	= jQuery(this);
		if(element.hasClass('opened')){
			element.removeClass('opened');
			triggerMenu.removeClass('opened');
			triggerClose.removeClass('opened');
			dropdown.slideUp();
		}else{
			element.addClass('opened');
			triggerMenu.addClass('opened');
			triggerClose.addClass('opened');
			dropdown.slideDown();
		}
		return false;
	});
}
renex_mobile_menu();	
	
	
	/************************* Anchor ****************************/
	
	
	function renex_anchor(){
	
	jQuery('.renex_leftpart .inner .menu ul li a').off().on('click',function(e){
		e.stopPropagation();
		var element = jQuery(this);
		var url			= element.attr('href');
		if(url !== '#' && url.charAt(0) === '#'){
			$('html, body').animate({
				scrollTop: $(url).offset().top-90
			}, 1000);
		}
		return false;
	});
}
renex_anchor();
	
	/************************* Menu Scroll *********************/
	
	
	function renex_menu_scrollable(){
		
	var H				= jQuery(window).height();
	var scrollable		= jQuery('.renex_leftpart .scrollable');
	var verMenu			= jQuery('.renex_leftpart .menu');
	var logoHeight		= jQuery('.renex_leftpart .logo').outerHeight();
	var audioHeight 	= jQuery('.renex_leftpart .bottom').outerHeight();

	verMenu.css({height:H-200-logoHeight-audioHeight});
	
	scrollable.each(function(){
		var element		= jQuery(this);
		
		element.css({height: H-200-logoHeight-audioHeight}).niceScroll({
			touchbehavior:false,
			cursorwidth:0,
			autohidemode:true,
			cursorborder:"0px solid #eee"
		});
	});
}
	renex_menu_scrollable();
	
	
	/************************* Appear ****************************/
	
	
	function renex_appear(){
		
	var div		= jQuery('.renex_appear');
	
	div.each(function(){
		
		var element	= jQuery(this);
		
		element.waypoint({
			handler:function(){
				element.addClass('load');
			},
			offset:"60%"
		});
		
	});
	
}
renex_appear();
	
	function renex_popup(){
	
		jQuery('.gallery_zoom').each(function() { // the containers for all your galleries
			jQuery(this).magnificPopup({
				delegate: 'a.zoom', // the selector for gallery item
				type: 'image',
				gallery: {
				  enabled:true
				},
				removalDelay: 300,
				mainClass: 'mfp-fade'
			});

		});
	}
	renex_popup();
	
	
	/************************* Ripple ****************************/
	
	
	function renex_ripple(){
		
		jQuery('#ripple').ripples({
				resolution: 500,
				dropRadius: 20,
				perturbance: 0.04
			});
	}
	renex_ripple();
	
	
	/************************* Video ****************************/
	
	
	$(".youtube-bg").mb_YTPlayer();
	
	
	/************************* Glitch ****************************/
	
	
	$(".glitch").mgGlitch({
		destroy: false,
		glitch: true,
		scale: true,
		blend: true,
		blendModeType: "hue",
		glitch1TimeMin: 200,
		glitch1TimeMax: 400,
		glitch2TimeMin: 10,
		glitch2TimeMax: 100
	});
	
	
	/************************* Hero Effect ****************************/
	
	
	function renex_hero_effect(){
		
	jQuery(window).on('scroll',function(){
		var currentScroll		= window.pageYOffset;
		jQuery(".renex_hero_wrap,.glitch").css({'transform': 'scale('+(100 - currentScroll/100)/99+')','opacity' : (1 - (currentScroll/20) / 15)});
		if(1 - (currentScroll/20) / 15 < 0){
			jQuery(".renex_hero_wrap,.glitch").parent().addClass('closed');
		}else{
			jQuery(".renex_hero_wrap,.glitch").parent().removeClass('closed');
		}
	});	
}
	
	/************************* Animate Text ****************************/
	
	function renex_animate_text(){
		
	var animateSpan			= jQuery('.arlo_tm_animation_text_word');
	
		animateSpan.typed({
		    strings: ["Medicine", "Cosmetics", "Health Products"],
			loop: true,
			startDelay: 1e3,
			backDelay: 2e3
		});
}
renex_animate_text();
	
	
	/************************* Preloader ****************************/
	
	
	function renex_preloader(){
		
	var mainPreloader	 = $(".renex_loader-wrapper .loader");
	var WinWidth 		 = $(window).width();
    var WinHeight		 = $(window).height();
    var zero = 0;

    mainPreloader.css({
        top: WinHeight / 2 - 2.5,
        left: WinWidth / 2 - 200
    });

    do {
        mainPreloader.animate({
            width: zero
        }, 10);
        zero += 3;
    } while (zero <= 400);
    if (zero === 402) {
        mainPreloader.animate({
            left: 0,
            width: '100%'
        });
        mainPreloader.animate({
            top: '0',
            height: '100vh'
        });
    }
		
    setTimeout(function() {
        $(".renex_loader-wrapper").fadeOut('fast');
        (mainPreloader).fadeOut('fast');
    }, 4500);
}
	jQuery(window).on('scroll',function(){
		renex_hero_effect();
		renex_menu_scrollable();
		renex_modal_scroll();
	});

	jQuery(window).on('resize',function(){
		renex_hero_height();
	});

	jQuery(window).load('body', function(){
		setTimeout(function(){renex_preloader();},1000);
	});
	
	
	/*********************** Modal News *************************/
	
	
	function renex_modal_news(){
		
		var modalBox		= jQuery('.renex_modal_news');
		var modalMain		= jQuery('.renex_modal_news .modal_main');
		var list			= jQuery('.renex_news .news_list ul li');
		var close 			= jQuery("#floatingmes");
		
		list.each(function(){
			var element		= jQuery(this);
			var button		= element.find('.renex_button a,.texts .title h3 a,.image a');
			var html		= element.html();
			var title		= element.find('.title h3');
			var titleHref	= element.find('.title h3 a').html();
			
			button.on('click',function(){
				modalBox.addClass('opened');
				modalMain.html(html);
				title = modalMain.find('.title h3');
				title.html(titleHref);
				modalBox.on('mousemove',function(pos){
					close.show(); 
					close.css('left',(pos.pageX+10)+'px').css('top',(pos.pageY+10)+'px'); 	
				}).on('mouseleave',function() {
					close.hide();
				});
				return false;
			});
		});
		modalBox.on('click',function(){
			var element = jQuery(this);
			element.removeClass('opened');
			element.scrollTop(0);
			close.hide();
		});
	}
	renex_modal_news();

	
	/************************* Modal Scroll *********************/
	
	
	function renex_modal_scroll(){
		
	var H				= jQuery(window).height();
	var scrollable		= jQuery('.renex_modal_news.scrollable');
	
	var popupBox		= jQuery('.renex_modal_news');
	
	popupBox.css({height:H});
	
	scrollable.each(function(){
		var element		= jQuery(this);
		var wH			= jQuery(window).height();
		
		element.css({height: wH});
		
		element.niceScroll({
			touchbehavior:false,
			cursorwidth:0,
			autohidemode:true,
			cursorborder:"0px solid #fff"
		});
	});
}
	renex_modal_scroll();
	
	
	/************************* Music ****************************/
	
jQuery('body.music').append('<audio loop autoplay volume="0" id="audio-player"><source src="audio/2.mp3" type="audio/mpeg"></audio>');
    var audio2 = jQuery("#audio-player");
    audio2.volume = 0.2;

function renex_music(){
      jQuery('.renex_music').css({'visibility':'visible'});
      jQuery('body').addClass("audio-on");
      if (jQuery('body').hasClass('audio-off')) {
        jQuery('body').removeClass('audio-on');
      } 
      jQuery(".renex_music").on('click', function() {
          jQuery('body').toggleClass("audio-on audio-off");         
          if (jQuery('body').hasClass('audio-off')) {
            audio2[0].pause();
          } 
          if (jQuery('body').hasClass('audio-on')) {
            audio2[0].play();
          }
      });

}
renex_music();
	
function renex_location(){
	var button		= jQuery('.href_location');
	button.on('click',function(){
		var element		= jQuery(this);
		var address		= element.text();
		address			= address.replace(/\ /g,'+');
		var text		= 'https://maps.google.com?q=';
		window.open(text+address);
		return false;
	});
}
renex_location();	
	
	function renex_contact_form(){
		
	jQuery(".contact_form #send_message").on('click', function(){
		var inputName		= jQuery(".contact_form #name");
		var inputEmail		= jQuery(".contact_form #email");
		var inputMessage	= jQuery(".contact_form #message");
		var name 			= inputName.val();
		var email 			= inputEmail.val();
		var message 		= inputMessage.val();
		var subject 		= jQuery(".contact_form #subject").val();
		var success    	 	= jQuery(".contact_form .returnmessage").data('success');
		var errorCount		= 0;
		var emailRegex  	= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	
		jQuery(".contact_form .returnmessage").empty(); //To empty previous error/success message.
		//checking for blank fields
		inputName.removeClass('warning');
		inputEmail.removeClass('warning');
		inputMessage.removeClass('warning');
		if(name === ''){
			inputName.addClass('warning');
			errorCount++;
		}
		if(email === '' || !emailRegex.test(email)){
			inputEmail.addClass('warning');
			errorCount++;
		}
		if(message === ''){
			inputMessage.addClass('warning');
			errorCount++;
		}
		
		if(errorCount === 0){
			// Returns successful data submission message when the entered information is stored in database.
			jQuery.post("modal/contact.php",{ ajax_name: name, ajax_email: email, ajax_message:message, ajax_subject: subject}, function(data) {
				
				jQuery(".contact_form .returnmessage").append(data);//Append returned message to message paragraph
				
				
				if(jQuery(".contact_form .returnmessage span.contact_error").length){
					jQuery(".contact_form .returnmessage").slideDown(500).delay(2000).slideUp(500);	
				}else{
					jQuery(".contact_form .returnmessage").append("<span class='contact_success'>"+ success +"</span>");
//					jQuery(".contact_form .returnmessage").slideDown(500).delay(4000).slideUp(500);
				}
				
				if(data===""){
					jQuery("#contact_form")[0].reset();//To reset form fields on success
				}
				
			});
		}
		return false; 
	});
}
	renex_contact_form();
	
	
	
});