﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ramix.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Homepage 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        .centeredContent {
            text-align: center;
        }

        .pharmaSpan {
            font-family: "Saira";
            font-size: 14px;
            color: #ff214f;
            font-weight: 700;
            text-transform: uppercase;
        }

        .arabicContent {
            float: right;
            direction: rtl;
        }

        .renex_portfolio .portfolio_list ul li {
            width: 50%;
        }

            .renex_portfolio .portfolio_list ul li .inner .image_wrap .image {
                width: auto;
            }

                .renex_portfolio .portfolio_list ul li .inner .image_wrap .image .main {
                    background-size: contain;
                }

        .contactX.xcon-facebook {
            position: absolute;
            left: -1.3%;
            color: #ff214f;
            font-size: 18pt;
        }

        #send_message {
            text-decoration: none;
            color: #fff;
            display: inline-block;
            background-color: #ff214f;
            font-weight: 500;
            padding: 10px 30px;
            border-radius: 5px;
            font-family: "Saira";
        }

        .renex_hero_wrap {
            background-size: contain;
            background-color: rgb(0, 0, 0);
        }

        @media only screen and (max-width: 641px) {
            .renex_section.top {
                display: inline-block !important;
                z-index: -21 !important;
            }

                .renex_section.top.closed {
                    display: none !important;
                }

            #about, #products, #contact, .renex_footer {
                z-index: 123456;
                background: white;
            }

            .renex_footer {
                background: #222;
            }

            .renex_hero_wrap {
                /*display: none !important;
                height: 50px !important;*/
                background-color: black !important;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="ohHide">
        <asp:TextBox runat="server" ID="txtFrom" Text="admin@ramixpharma.com" />
        <asp:TextBox runat="server" ID="txtPass" TextMode="Password" Text="" />
        <asp:TextBox runat="server" ID="txtMailServer" Text="mail.ramixpharma.com" />
        <asp:TextBox runat="server" ID="txtTo" Text="admin@ramixpharma.com" />
    </div>
    <div class="top_border"></div>
    <div class="rightpart_inner">

        <!-- Hero -->
        <div class="renex_section top" id="home">
            <div class="renex_hero_wrap" data-img-url="images/ramix-banner.png">
                <div class="overlay ohHide"></div>
                <div class="hero_texts ohHide">
                    <div class="hero_image">
                        <img src="images/about/500x500.jpg" alt="" />
                        <img class="img img-responsive" src="images/logo.png" style="border-radius: 25% !important; margin: 15% 0;" />
                    </div>
                    <h3 class="name">Ramix <span class="surname">Pharma Company</span></h3>
                    <div class="www ohHide"><span class="subtitle">We are specialized in <span class="arlo_tm_animation_text_word"></span></span></div>
                </div>
                <div class="renex_down">
                    <div class="line_wrapper">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Hero -->

        <!-- About -->
        <div class="renex_section centeredContent" id="about">
            <div class="container">
                <div class="renex_title_holder">
                    <span>About us</span>
                </div>
                <div class="renex_about">
                    <div class="text">
                        <p><span class="pharmaSpan">#Ramix_pharma</span> is one of the leading of companies in Egypt. It has a distinct customer base by delivering a sustainable business. It offers health benefits to patients and consumers as well as supporting wider society.</p>
                        <p><span class="pharmaSpan">#Ramix_pharma</span> has self-committed to present the highest quality of medicines to the patients. We have a principle of self-challenging to offer the competitive environment and do all the efforts to satisfy patients, employees, business partners, suppliers, shareholders and the whole community.</p>
                        <p><span class="pharmaSpan">#Ramix_pharma</span> teams of professionals who has both of skills and knowledge, is working in a harmony to achieve our goals and bring the dreams into reality. </p>
                        <p><span class="pharmaSpan">#Ramix_pharma</span> was not only equipped with up-to-date technology and production facilities, but also the whole organization is supported by the sustainable development which guided by the long-term business strategy to ensure best quality.</p>
                        <p><span class="pharmaSpan">#Ramix_pharma</span> is one of the companies which specialized in sales and marketing activities. We do our best to offer continuous efforts to help our community to live a better and healthier life.</p>
                        <br />
                        <div class="arabicContent ohHide">
                            <p>
                                <span class="pharmaSpan">رامكس فارما</span> هي شركة مصرية رائدة فى مجال الادوية والمستحضرات الطبيه وتعتمد على التفانى والجديه فى العمل وتقدر عملائها المميزين من خلال تقديم اعمال مستدامة
                            </p>
                            <p>
                                نحن نقدم الفوائد الصحية للمريض والمستهلكين , نريد ان نساعد الناس لا اكثر بحيث يشعر على نحو افضل ويعيش لفترة اطول وبصحة افضل
                            </p>
                            <p>
                                اليوم لا يزال هناك الملايين من الاشخاص الذين لا يحصلون على الرعاية الصحية الاساسية ,والالاف من الامراض دون العلاج الكافى والملايين من الناس الذين يعانون  من امراض المزمنة فى رامكس فارما نريد تغيير هذا
                            </p>
                            <p>
                                وتقوم الشركة ببذل قصارى جهدها فى البحث العلمى والتصنيع لتقديم كل ما يحتاجة السوق المصرى "المواطن المصرى" من ادوية ومستحضرات طبيه , ونحن ملتزمون فى توسيع فرص الحصول على منتجاتنا حتى اكثر الناس يمكن ان تستفيد , بغض النظر عن المكان الذي يعيشون به او ما يستطيعوا ان يدفعوه
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /About -->

        <!-- Portfolio -->
        <div class="renex_section ohHide" id="products">
            <div class="renex_portfolio">
                <div class="container">
                    <div class="renex_title_holder">
                        <span>Products</span>
                    </div>
                    <div class="portfolio_list renex_appear">
                        <ul class="gallery_zoom">
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="images/1.jpeg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Duval</h3>
                                    </div>
                                    <a class="link zoom" href="images/1Zoom.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="images/2.png"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Right Mix Spray</h3>
                                    </div>
                                    <a class="link zoom" href="images/4Zoom.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="images/3.png"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>OP Skin</h3>
                                    </div>
                                    <a class="link zoom" href="images/3Zoom.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="images/4.png"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Right Mix  Massage Gel</h3>
                                    </div>
                                    <a class="link zoom" href="images/2Zoom.jpg"></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Portfolio -->

        <!-- Contact -->
        <div class="renex_section" id="contact">
            <div class="renex_contact">
                <div class="container">
                    <div class="renex_title_holder centeredContent">
                        <span>Contact</span>
                    </div>
                    <div class="contact_inner">
                        <div class="left">
                            <ul>
                                <li>
                                    <div class="inner">
                                        <i class="contactX xcon-facebook"></i>
                                        <span><a target="_blank" href="https://www.facebook.com/RamixPharma/">Ramix Pharma</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/landline.svg" alt="" />
                                        <span><a href="tel:0221806905">0221806905</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/telephone.svg" alt="" />
                                        <span><a href="tel:+2 01028221991">+2 01028221991</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/envelope.svg" alt="" />
                                        <span><a href="mailto:RamixPharma@gmail.com">RamixPharma@gmail.com</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/whatsapp.svg" alt="" />
                                        <span><a href="https://wa.me/+201028221991" target="_blank">01028221991</a></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="right">
                            <form class="contact_form" id="contact_form">
                                <div class="returnmessage" data-success="Your message has been received, We will contact you soon."></div>
                                <div class="first_row">
                                    <ul>
                                        <li>
                                            <asp:TextBox ID="name" ClientIDMode="Static" placeholder="Your Name" runat="server" />
                                            <asp:RequiredFieldValidator ID="nameVali" ControlToValidate="name" runat="server" ErrorMessage="name is required"></asp:RequiredFieldValidator>
                                        </li>
                                        <li>
                                            <asp:TextBox runat="server" placeholder="Your Email" ID="email" ClientIDMode="Static" />
                                            <asp:RequiredFieldValidator ID="mailVali" ControlToValidate="email" runat="server" ErrorMessage="email is required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="isMailVali" ControlToValidate="email" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" runat="server" ErrorMessage="please use a valid email"></asp:RegularExpressionValidator>
                                        </li>
                                    </ul>
                                </div>
                                <div class="second_row">
                                    <asp:TextBox TextMode="MultiLine" placeholder="Your Message" ID="txtMessage" ClientIDMode="Static" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="messageVali" ControlToValidate="txtMessage" runat="server" ErrorMessage="Message is required"></asp:RequiredFieldValidator>
                                </div>
                                <div class="renex_button centeredContent">
                                    <asp:Button Text="Submit" ID="send_message" ClientIDMode="Static" CssClass="btn btn-success" runat="server" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="out"></div>
        <!-- /Contact -->

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        $(document).ready(function () {

            $("#send_message").click(function () {
                sendAjaxRequest();
            });



            function sendAjaxRequest() {
                var NameForm = $("#name").val();
                var EmailForm = $("#email").val();
                var MsgForm = $("#txtMessage").val();
                var BodyForm = "From: " + NameForm + "\n";
                BodyForm += "Email: " + EmailForm + "\n";
                BodyForm += "Subject: Email from Ramix \n";
                BodyForm += "Quick Description: \n" + MsgForm + "\n";
                $.ajax({
                    async: true,
                    url: "WebService.asmx/SendMail",
                    type: "POST",
                    dataType: "json",
                    data: '{bodyForm: "' + BodyForm + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: responseOut,
                    error: function (error) {
                        debugger;
                        //alert(error);
                    }
                });
            };


            function responseOut(response) {
                //$("#out").html(response.d);
                window.location.href = "Default.aspx";
            }

        });

    </script>
</asp:Content>
