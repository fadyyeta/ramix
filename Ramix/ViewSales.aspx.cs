﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class ViewSales : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadInvoice();
            }
        }
        protected void loadInvoice()
        {
            DataSet ds = codeClass.SQLREAD("SELECT  Invoice.ID, Invoice.invoiceName, [User].fullName as UserName, Invoice.pharmacyName, Invoice.site, Areas.name as area, Invoice.ImageURL as ImageURL, Invoice.numberOfUnitsSold, Invoice.packagePrice, Invoice.discount, Invoice.bonus, Invoice.billNumber,  Product.name , Invoice.paid, Invoice.dateOrder, Invoice.total FROM Invoice INNER JOIN  [User] ON Invoice.userID = [User].ID INNER JOIN Areas ON Invoice.area = Areas.ID INNER JOIN Product ON Invoice.productID = Product.ID");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }
        protected void loadVisit()
        {
            DataSet ds = codeClass.SQLREAD("SELECT * from Visit");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }
    }

}