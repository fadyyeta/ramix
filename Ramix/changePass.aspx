﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ChangePass.aspx.cs" Inherits="Ramix.ChangePass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Change Password
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        .card.card-login.card-hidden {
            width: 50%;
            text-align: center;
            margin: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="card card-login card-hidden">
        <div class="card-header card-header-rose text-center">
            <h4 class="card-title">Change Password</h4>
        </div>
        <div class="card-body " style="padding: 0 5%;">
            <div class="text-center">
                <div class="alert alert-danger" runat="server" id="wrong">
                    <p>Sorry wrong credentials. please try again</p>
                </div>
            </div>
            <span class="bmd-form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">email</i>
                        </span>
                    </div>
                    <asp:TextBox ID="txtEmail" CssClass="form-control" placeholder="Email..." Enabled="false" TextMode="Email" runat="server" />
                </div>
            </span>
            <span class="bmd-form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">lock_outline</i>
                        </span>
                    </div>
                    <asp:TextBox CssClass="form-control" placeholder="Password..." TextMode="Password" ID="txtPass" runat="server" />
                </div>
            </span>
        </div>
        <div class="card-footer justify-content-center">
            <asp:Button Text="Change" CssClass="btn btn-rose btn-link btn-lg" ID="btnChange" OnClick="btnChange_Click" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
